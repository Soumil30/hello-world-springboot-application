FROM openjdk:15

WORKDIR /usr/src/app/

COPY gradle gradle
COPY gradlew gradlew
COPY build.gradle build.gradle
COPY src src

RUN ./gradlew build

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "./build/libs/hello-world-0.1.0.jar"]